const fs = require('fs');
const path = require('path');
const mm = require('music-metadata');
const FfmpegPeaks = require('ffmpeg-peaks');

const pathFile = path.join(__dirname, '../data/3.mp3');

const ffpeaks = new FfmpegPeaks({
  width: 1640,
  precision: 1,
  numOfChannels: 2,
  sampleRate: 44100,
});

ffpeaks.getPeaks(pathFile, (err, peaks) => {
  if (err) return console.error(err.message);
  mm.parseFile(pathFile, { duration: true, native: true })
    .then((metadata) => {
      metadata.waveform = peaks;
      fs.writeFile('pars.json', JSON.stringify(metadata, null, 4), (err1) => {
        if (err1) throw err1;
        console.log('The file has been saved!');
      });
    })
    .catch((err1) => {
      console.error(err1.message);
    });
});
